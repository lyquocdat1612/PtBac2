package giaiPt;

public class GiaiHePtBac2 {
	public Float tinhtoan(int a, int b, int c) {
		float delta = b * b - 4 * a * c;
		float x1;
		float x2;
		float sum;
		x1 = (float) ((-b + Math.sqrt(delta)) / (2*a));
        x2 = (float) ((-b - Math.sqrt(delta)) / (2*a));
        System.out.println("Nghiem cua phuong trinh : x1 = " + x1);
		System.out.println("Nghiem cua phuong trinh : x2 = " + x2);
		return x1+x2;
	}
	public static void main (String []args) {
		GiaiHePtBac2 abc = new GiaiHePtBac2();
		float sum = abc.tinhtoan(1, -5, 6);
		System.out.println("tong cua 2 nghiem la : " + sum);
		
	}
}